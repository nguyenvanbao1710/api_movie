const { PrismaClient } = require('@prisma/client');
const {
  successCode,
  errorCode,
  failCode
} = require('../comon/response');

const prisma = new PrismaClient();

const getBanners = async (req, res) => {
  try {
    const response = await prisma.banner.findMany();
    return successCode(res, response);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const getMovies = async (req, res) => {
  try {
    const response = await prisma.movie.findMany();
    return successCode(res, response);
  } catch (error) {
    return failCode(res);
  }
};

const searchMovies = async (req, res) => {
  try {
    const {
      keywords = '',
      page = 1,
      size = 20,
    } = req.query;

    const response = await prisma.movie.findMany({
      skip: size * page - size,
      take: Number(size),
      where: {
        name: {
          contains: `${keywords}`,
        },
      },
    });
    return successCode(res, response);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const getMovieByDate = async (req, res) => {
  try {
    const {
      fromDate,
      toDate,
    } = req.query;
    const response = await prisma.movie.findMany({
      where: {
        startDate: {
          lte: new Date(toDate),
          gte: new Date(fromDate),
        },
      },
    });
    return successCode(res, response);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const uploadPoster = async (req, res) => {
  try {
    const { movieId } = req.query;
    const files = req.file;

    const movie = await prisma.movie.findFirst({
      where: {
        id: Number(movieId),
      },
    });
    if (!movie) return errorCode(res, 'Phim không tồn tại vui lòng thử lại.');
    if (!files) return errorCode(res, '');

    const dir = `${__dirname}/uploads/${req.file.filename}`;
    res.sendFile(dir);
    const response = await prisma.movie.update({
      data: {
        poster: dir,
      },
      where: {
        id: Number(movieId),
      },
    });
    return successCode(res, response);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const editMovie = async (req, res) => {
  try {
    const {
      name,
      startDate,
      time,
      evaluate,
      poster,
      trailer,
    } = req.body;

    const { id } = req.query;
    const movie = await prisma.movie.findFirst({
      where: {
        id,
      },
    });
    if (!movie) return errorCode(res, 'Dữ liệu không hợp lệ. Vui lòng thử lại');
    const response = await prisma.movie.update({
      data: {
        name,
        startDate: new Date(startDate).toISOString(),
        time: new Date(time).toISOString(),
        evaluate,
        poster,
        trailer,
      },
      where: {
        id,
      },
    });
    return successCode(res, response);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const createMovie = async (req, res) => {
  try {
    const {
      name,
      startDate,
      time,
      evaluate,
      poster,
      trailer,
    } = req.body;
    const response = await prisma.movie.create({
      data: {
        name,
        startDate: new Date(startDate),
        time: new Date(time),
        evaluate,
        poster,
        trailer,
      },
    });
    return successCode(res, response);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const deleteMovie = async (req, res) => {
  try {
    const { id } = req.query;
    const response = await prisma.movie.delete({
      where: {
        id: Number(id),
      },
    });
    successCode(res, response);
  } catch (error) {
    console.log(error);
    failCode(res);
  }
};

module.exports = {
  getBanners,
  getMovies,
  searchMovies,
  getMovieByDate,
  createMovie,
  editMovie,
  uploadPoster,
  deleteMovie,
};

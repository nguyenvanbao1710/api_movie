const { PrismaClient } = require('@prisma/client');
const {
  successCode,
  failCode,
} = require('../comon/response');

const prisma = new PrismaClient();

const getCinemas = async (req, res) => {
  try {
    const response = await prisma.cinema.findMany();
    successCode(res, response);
  } catch (error) {
    failCode(res);
  }
};

const getCineFlex = async (req, res) => {
  try {
    const { id } = req.query;
    const cineflex = await prisma.cineflex.findFirst({ where: { id: Number.parseInt(id, 10) } });
    return successCode(res, cineflex);
  } catch (e) {
    return failCode(res);
  }
};

const getMovieShowTime = async (req, res) => {
  try {
    const { movieId } = req.query;
    const movie = await prisma.movie.findFirst({
      where: {
        id: movieId,
      },
    });
    return successCode(res, movie.showtime);
  } catch (e) {
    return failCode(res);
  }
};

const createCinemaMovie = async (req, res) => {
  try {
    const {
      cinemaId,
      movieId,
    } = req.body;

    const result = await prisma.cinemaMovie.create({
      data: {
        cinemaId,
        movieId,
      },
    });
    return successCode(res, result);
  } catch (e) {
    return failCode(res);
  }
};

module.exports = {
  getCinemas,
  getCineFlex,
  getMovieShowTime,
  createCinemaMovie,
};

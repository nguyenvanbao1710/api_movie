const { PrismaClient } = require('@prisma/client');
const moment = require('moment');
const { prefix } = require('prisma/preinstall');
const {
  successCode,
  failCode,
  errorCode,
} = require('../comon/response');
const constant = require('../comon/constant');

const prisma = new PrismaClient();

const createShowTime = async (req, res) => {
  try {
    const {
      cinemaId,
      movieId,
      startTime,
    } = req.body;
    const cinemaMovie = await prisma.cinemaMovie.findFirst({
      where: {
        cinemaId,
        movieId,
      },
    });

    if (!cinemaMovie) {
      return res.status(400)
        .json({
          error: true,
          message:
            'Không có rạp phim và phim theo dữ liệu. Vui lòng thử lại',
        });
    }
    const timeFormat = moment(startTime)
      .toISOString();
    const isHaveTime = await prisma.showtime.findFirst({
      where: {
        startTime: timeFormat,
      },
    });
    if (isHaveTime) {
      return res.status(400)
        .json({
          error: false,
          message: 'Đã có lịch chiếu vào thời gian trên. Vui lòng thử lại',
        });
    }

    const resp = await prisma.showtime.create({
      data: {
        startTime: timeFormat,
        cinemaId,
      },
    });
    if (resp) {
      return successCode(res, resp);
    }
    return res.status(400)
      .json({
        error: true,
        message: 'Không thể tạo lịch chiếu. Vui lòng thử lại',
      });
  } catch (error) {
    console.log(error);
    failCode(res);
  }
};

const bookingTicket = async (req, res) => {
  try {
    const {
      ticketLists,
      showTimeId,
    } = req.body;
    const showTime = prisma.showtime.findFirst({
      where: {
        id: showTimeId,
      },
    });
    if (showTime) {
      const promise = ticketLists.map(async (item) => {
        const availableSeat = await prisma.seat.findFirst({
          where: {
            name: item.name,
          },
        });
        return prisma.seat.update({
          where: {
            id: availableSeat.id,
          },
          data: {
            status: !availableSeat.status,
          },
        });
      });
      const result = await Promise.all(promise);
      return successCode(res, {
        showTimeId,
        ticketLists: result,
      });
    }
    return res.status(400)
      .json({
        error: true,
        message: 'Không tìm thấy lịch chiếu. Vui lòng thử lại',
      });
  } catch (error) {
    failCode(res);
  }
};

const getCinemasByShowTime = async (req, res) => {
  try {
    const { showTimeId } = req.query;
    const showTime = await prisma.showtime.findFirst({
      where: {
        id: Number.parseInt(showTimeId, 10),
      },
    });
    const listShowTime = await prisma.showtime.findMany({
      where: {
        startTime: showTime.startTime,
      },
    });

    const cinemasPromise = listShowTime.map((show) => prisma.cinema.findFirst({ where: { id: show.cinemaId } }));
    const cinemas = await Promise.all(cinemasPromise);
    return successCode(res, cinemas);
  } catch (error) {
    return failCode(res);
  }
};

const getShowTimes = async (req, res) => {
  try {
    const showTimes = await prisma.showtime.findMany();
    return successCode(res, showTimes);
  } catch (e) {
    return failCode(res);
  }
};

const createSeat = async (req, res) => {
  try {
    const { name } = req.body;

    const currentSeat = await prisma.seat.findFirst({ where: { name } });
    if (currentSeat) {
      return errorCode(res, constant.FAIL);
    }
    const seat = await prisma.seat.create({ data: req.body });
    return successCode(res, seat);
  } catch (e) {
    return failCode(res);
  }
};

module.exports = {
  createShowTime,
  bookingTicket,
  getCinemasByShowTime,
  getShowTimes,
  createSeat,
};

const { PrismaClient } = require('@prisma/client');
require('dotenv');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const {
  successCode,
  errorCode,
  failCode,
} = require('../comon/response');

const {
  checkToken,
  checkPasswordRequirements
} = require('../comon/utils');
const constant = require('../comon/constant');

const prisma = new PrismaClient();
const saltRounds = 10;

const getRoleLists = async (req, res) => {
  try {
    const response = await prisma.role.findMany();
    successCode(res, response);
  } catch (error) {
    console.log(error);
    failCode(res);
  }
};

const getUserLists = async (req, res) => {
  try {
    const response = await prisma.user.findMany();
    return successCode(res, response);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const searchUser = async (req, res) => {
  try {
    const {
      keywords = '',
      page = 1,
      size = 20,
    } = req.query;
    const response = await prisma.user.findMany({
      skip: size * page - size,
      take: Number(size),
      where: {
        name: {
          contains: `${keywords}`,
        },
      },
    });
    successCode(res, response);
  } catch (error) {
    console.log(error);
    failCode(res);
  }
};

const register = async (req, res) => {
  try {
    const {
      name,
      email,
      phone,
      password,
      roleId,
    } = req.body;
    const checkEmailAndPhone = await prisma.user.findFirst({
      where: {
        OR: [{ email }, { phone }],
      },
    });
    if (checkEmailAndPhone) {
      return errorCode(res, constant.EMAIL_OR_PHONE_EXIST);
    }
    if ((roleId && !req.user) || (roleId && !req.isAdmin)) {
      return errorCode(res, constant.UNAUTHORIZED);
    }
    const checkPassword = checkPasswordRequirements(password);
    if (!checkPassword) {
      return errorCode(res, 'Password');
    }
    const roleUser = await prisma.role.findFirst({ where: { role: 'user' } });
    bcrypt.hash(password, saltRounds, async (err, hash) => {
      if (err) return failCode(res);
      const response = await prisma.user.create({
        data: {
          name,
          email,
          phone,
          password: hash,
          roleId: roleId || roleUser.id,
        },
      });
      return successCode(res, response);
    });
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

const login = async (req, res) => {
  try {
    const {
      phone,
      password,
    } = req.body;
    const user = await prisma.user.findFirst({
      where: {
        phone,
      },
    });
    if (!user) return errorCode(res, constant.PHONE_NOT_EXIST);
    const checkPassword = bcrypt.compareSync(password, user.password);
    if (!checkPassword) {
      return errorCode(res, constant.USER_NOT_EXIST);
    }
    const token = await jwt.sign({
      data: {
        name: user.name,
        email: user.email,
        phone: user.phone,
        role: user.roleId,
      },
    }, process.env.SECRET_KEY_JWT, { expiresIn: process.env.TIME_EXPIRED });
    return successCode(res, {
      message: constant.SUCCESS,
      token,
    });
  } catch (error) {
    return failCode(res);
  }
};

const validateToken = async (req, res) => {
  try {
    const { token } = req.body;
    const isValid = await checkToken(token);
    if (isValid.error) {
      return errorCode(res, isValid.err);
    }
    return successCode(res, isValid.decoded);
  } catch (error) {
    return failCode(res);
  }
};

const editUser = async (req, res) => {
  try {
    const {
      name,
      email,
      phone,
      password,
      roleId,
    } = req.body;
    const { userId } = req.query;
    const user = await prisma.user.findFirst({
      where: {
        id: Number.parseInt(userId, 10),
      },
    });
    if (!user) {
      return errorCode(res, constant.USER_NOT_EXIST);
    }

    if (user.id !== req.user.id && !req.isAdmin) {
      return errorCode(res, constant.UNAUTHORIZED);
    }

    if (roleId && !req.isAdmin) {
      return errorCode(res, constant.UNAUTHORIZED);
    }
    if (password) {
      const result = checkPasswordRequirements(password);
      if (!result) {
        return errorCode(res, 'Password ');
      }
    }
    const hash = password ? bcrypt.hashSync(password, saltRounds) : password;
    const userUpdated = await prisma.user.update({
      data: {
        name: name || undefined,
        password: hash || undefined,
        email: email || undefined,
        phone: phone || undefined,
        roleId: roleId || undefined,
      },
      where: {
        id: user.id,
      },
    });
    return successCode(res, userUpdated);
  } catch (error) {
    console.log(error);
    return failCode(res);
  }
};

module.exports = {
  getRoleLists,
  getUserLists,
  searchUser,
  register,
  login,
  validateToken,
  editUser,
};

const { PrismaClient } = require('@prisma/client');
const { failCode } = require('../comon/response');
const { checkToken } = require('../comon/utils');
const constant = require('../comon/constant');

const prisma = new PrismaClient();

const protectedRoute = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    if (!token) {
      return res.status(403)
        .json({
          error: true,
          message: constant.UNAUTHORIZED,
        });
    }
    // eslint-disable-next-line prefer-destructuring
    token = token.includes('Bearer') ? token.split(' ')[1] : token;
    const result = await checkToken(token);
    if (result.error) {
      if (result.expiredAt) {
        return res.status(401)
          .json({
            error: true,
            message: constant.EXPIRED_TOKEN,
          });
      }
      return res.status(401)
        .json({
          error: true,
          message: constant.INVALID_TOKEN,
        });
    }
    const { data } = result.decoded;
    const user = await prisma.user.findFirst({
      where: {
        email: data.email,
        phone: data.phone,
      },
    });
    if (!user) {
      return res.status(400)
        .json({
          error: true,
          message: constant.UNAUTHORIZED,
        });
    }
    const userRole = await prisma.role.findFirst({
      where: {
        id: user.roleId,
      },
    });
    const isAdmin = userRole.name === 'admin';

    req.user = user;
    req.isAdmin = isAdmin;
    return next();
  } catch (error) {
    return failCode(res);
  }
};

module.exports = protectedRoute;

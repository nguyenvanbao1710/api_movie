const SUCCESS = 'Thành công';
const FAIL = 'Thất bại';
const INTERNAL_ERROR = 'Lỗi hệ thống!';
const UNAUTHORIZED = 'Yêu cầu không hợp lệ!';

const INVALID_TOKEN = 'Token không hợp lệ. Vui lòng đăng nhập lại.';
const EXPIRED_TOKEN = 'Token hết hạn';
const EMAIL_OR_PHONE_EXIST = 'Email/Số điện thoại đã tồn tại';
const PHONE_NOT_EXIST = 'Số điện thoại không tồn tại';
const USER_NOT_EXIST = 'Số điện thoại/Mật khẩu không đúng. Vui lòng thử lại';
const USER_ACCOUNT_NOT_EXIST = 'Không có user trong hệ thống. Vui lòng thử lại';

const PASSWORD_REGEX = '^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$';

module.exports = {
  SUCCESS,
  FAIL,
  INTERNAL_ERROR,
  INVALID_TOKEN,
  EMAIL_OR_PHONE_EXIST,
  EXPIRED_TOKEN,
  PHONE_NOT_EXIST,
  USER_NOT_EXIST,
  USER_ACCOUNT_NOT_EXIST,
  UNAUTHORIZED,
  PASSWORD_REGEX,
};

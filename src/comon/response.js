const constant = require('./constant');

const successCode = (res, data) => {
  const response = {
    message: constant.SUCCESS,
    data,
  };

  res.status(200).send(response);
};

const errorCode = (res, data) => {
  const response = {
    message: constant.FAIL,
    data,
  };
  res.status(400).send(response);
};

const failCode = (res) => {
  const response = {
    message: constant.INTERNAL_ERROR,
  };
  res.status(500).send(response);
};

module.exports = {
  successCode,
  errorCode,
  failCode,
};

const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
const constant = require('./constant');

const convertBase64 = (file) => file.buffer.toString('base64');

dotenv.config();
const checkToken = async (token) => jwt.verify(
  token,
  process.env.SECRET_KEY_JWT,
  (err, decoded) => {
    if (err) {
      return {
        error: true,
        err,
      };
    }
    return {
      error: false,
      decoded,
    };
  },
);

const checkPasswordRequirements = (password = '') => {
  const regex = new RegExp(constant.PASSWORD_REGEX);
  return regex.test(password);
};
module.exports = {
  convertBase64,
  checkToken,
  checkPasswordRequirements,
};

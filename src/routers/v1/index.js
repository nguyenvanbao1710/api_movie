const express = require('express');
const protectedRoute = require('../../middlewares/protectRoute');

const bookingRouter = require('./booking');
const movieRouter = require('./movie');
const cinemaRouter = require('./cinema');
const userRouter = require('./user');

const v1Router = express.Router();

v1Router.use('/booking', protectedRoute, bookingRouter);
v1Router.use('/movies', protectedRoute, movieRouter);
v1Router.use('/cinemas', protectedRoute, cinemaRouter);

v1Router.use('/users', userRouter);

v1Router.use('/uploads', express.static('uploads'));

module.exports = { v1Router };

const express = require('express');
const cinemaController = require('../../controllers/cinema');

const cinemaRouter = express.Router();

cinemaRouter.get('/', cinemaController.getCinemas);
cinemaRouter.get('/flex', cinemaController.getCineFlex);
cinemaRouter.get('/showtime', cinemaController.getMovieShowTime);
cinemaRouter.post('/', cinemaController.createCinemaMovie);

module.exports = cinemaRouter;
